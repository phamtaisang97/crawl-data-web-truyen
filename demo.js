const puppeteer = require('puppeteer');

(async () => {
  // Launch a browser instance
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Specify the starting URL and element containing the target href
  const url = 'https://dtruyen.com/tien-hiep/';
  const elementSelector = '.story-list a'; // Adjust selector based on your page structure

  // Load the page
  await page.goto(url);

  // Find all elements matching the selector
  const targetElements = await page.$$(elementSelector);

  // Loop through each element and handle individually
  for (const element of targetElements) {
    // Extract the href attribute
    const detailUrl = await element.getProperty('href').then(handle => handle.jsonValue());

    // Navigate to the detail page and process information
    await navigateAndProcessDetail(detailUrl, page);
  }

  // Close the browser and clean up
  await browser.close();

  // Helper function for processing detail pages
  async function navigateAndProcessDetail(detailUrl, page) {
    await page.goto(detailUrl);

    // Use your desired `querySelector` and extract information
    const detailElement = await page.$$('.description');

    // Process the extracted information (e.g., print it to console)
    // console.log(`Description: ${detailElement}`);
  }
})();