const puppeteer = require('puppeteer');

let electronicUrl = 'https://dtruyen.com/tien-hiep/';
(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1200,
    height: 800
  });

  await page.goto(electronicUrl);
  let electronicData = await page.evaluate(async () => {
    let stories = [];
    let storiesWrapper = document.querySelectorAll('.story-list');

    for (const story of storiesWrapper) {
    // storiesWrapper.forEach(async (story) => {
      let dataJson = {};
  
      try {
        // Attempt data extraction within page.evaluate
        dataJson.image = story.querySelector('img').src;
        dataJson.title = story.querySelector('.info .title a').innerText;
        dataJson.href = story.querySelector('.info .title a').href;
      } catch (err) {
        console.log(err);
      }

      stories.push(dataJson);
    };
  
    return stories;
  });
  
  let result = [];
  for (const story of electronicData) {
    let dataJson = story;
    await page.goto(story.href);
    await page.waitFor(1000);
    const descriptionElements = await page.$$('.description');
    if(descriptionElements.length > 0) {
      const description =  await page.$eval('.description', el => el ? el.innerText : "");
      const author =  await page.$eval('.author a', el => el ? el.innerText : "");
      const author_url =  await page.$eval('.author a', el => el ? el.href : "");
      const story_categories =  await page.$eval('.story_categories a', el => el ? el.innerText : "");
      dataJson.description = description;
      dataJson.author = author;
      dataJson.author_url = author_url;
      dataJson.category = story_categories;
      let unitResult = await page.evaluate(async () => {
        let units = [];
        let unitsWrapper = document.querySelectorAll('#chapters .vip-0');
        for (const u of unitsWrapper) {
            let temp = {};
            try {
              // Attempt data extraction within page.evaluate
              if (u.querySelector('a').href.length == 0) {
                continue;
              }
              temp.href = u.querySelector('a').href;
              temp.text = u.querySelector('a').innerText;

              
              // await page.goto(u.querySelector('a').href);

              // go to page chapter_content https://dtruyen.com/mo-dau-nu-de-lam-chinh-cung/tieu-thien-so-vai-dai-ca-quan_5181457.html
              // let unitResult = await page.evaluate(async () => {
              //   result_chapter_contents = [];
              //   let chapter_contents = document.querySelectorAll('#chapter-content');
              //   for (const chapter_content of chapter_contents) {
              //     let tempV = {};
              //     tempV.content = "chapter_content";
              //     result_chapter_contents.push(tempV)
              //   }
              //   return result_chapter_contents;
              // })

              temp.chapter_contents = unitResult;

            } catch (err) {
              console.log(err);
            }

            units.push(temp);
        }
        return units
      });

      dataJson.href_unit = unitResult;
      result.push(dataJson);
    }
  }
  console.log(result[0].href_unit)
  await browser.close();
})();